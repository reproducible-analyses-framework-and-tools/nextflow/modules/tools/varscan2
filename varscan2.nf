#!/usr/bin/env nextflow

include { extract_chroms_from_bed } from '../ref_utils/ref_utils.nf'

include { htslib_bgzip_somatic as htslib_bgzip_somatic_snvs } from '../htslib/htslib.nf'
include { htslib_bgzip_somatic as htslib_bgzip_somatic_indels } from '../htslib/htslib.nf'

include { bcftools_index_somatic as bcftools_index_somatic_snvs } from '../bcftools/bcftools.nf'
include { bcftools_index_somatic as bcftools_index_somatic_indels } from '../bcftools/bcftools.nf'

include { bcftools_concat as bcftools_concat_snvs } from '../bcftools/bcftools.nf'
include { bcftools_concat as bcftools_concat_indels } from '../bcftools/bcftools.nf'

process varscan2_somatic {
// Runs varscan2 on a set of paired samples
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(dataset) - Dataset
//     val(norm_run) - Normal Run Name
//     path(norm_bam) - Normal Alignment File
//     path(norm_bai) - Normal Alignment Index
//     val(tumor_run) - Tumor Run Name
//     path(tumor_bam) - Tumor Alignment File
//     path(tumor_bai) - Tumor Alignment Index
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Reference Index Files
//     path(dict_file) - Reference Dict File
//   parstr - Additional Parameters
//
// output:
//   tuple
//     val(pat_name) - Patient Name
//     val(dataset) - Dataset
//     path('*vcf') - Variant Call File

// require:
//   NORM_TUMOR_BAMS_BAIS
//   REF_W_INDICES
//   params.varscan2$varscan2_somatic_parameters

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'varscan2_container'
  label 'varscan2_somatic'
  cache 'lenient'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/varscan2_somatic"


  input:
  tuple val(pat_name), val(dataset), val(norm_run), path(norm_pileup), val(tumor_run), path(tumor_pileup)
  val parstr
  val suffix

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*snp.vcf"), emit: snv_vcfs
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*indel.vcf"), emit: indel_vcfs

  script:
  """
  java -jar /opt/varscan/VarScan.jar somatic ${norm_pileup} ${tumor_pileup} ${dataset}-${pat_name}-${norm_run}_${tumor_run}${suffix} ${parstr}
  """
}

process varscan2_process_somatic {
// Runs varscan2 processSomatic on a VCF
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(dataset) - Dataset
//     val(norm_run) - Normal Run Name
//     path(norm_bam) - Normal Alignment File
//     path(norm_bai) - Normal Alignment Index
//     val(tumor_run) - Tumor Run Name
//     path(tumor_bam) - Tumor Alignment File
//     path(tumor_bai) - Tumor Alignment Index
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Reference Index Files
//     path(dict_file) - Reference Dict File
//   parstr - Additional Parameters
//
// output:
//   tuple
//     val(pat_name) - Patient Name
//     val(dataset) - Dataset
//     path('*vcf') - Variant Call File

// require:
//   NORM_TUMOR_BAMS_BAIS
//   REF_W_INDICES
//   params.varscan2$varscan2_somatic_parameters

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'varscan2_container'
  label 'varscan2_somatic'
  cache 'lenient'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/varscan2_somatic"


  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(vcf)
  val parstr
  val suffix

  output:
//  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*snp.vcf"), emit: snv_vcfs
//  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*indel.vcf"), emit: indel_vcfs

  script:
  """
  java -jar /opt/varscan/VarScan.jar processSomatic ${vcf} ${parstr}
  """
}

process varscan2_somatic_by_chr {
// Runs varscan2 on a set of paired samples
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(dataset) - Dataset
//     val(norm_run) - Normal Run Name
//     path(norm_bam) - Normal Alignment File
//     path(norm_bai) - Normal Alignment Index
//     val(tumor_run) - Tumor Run Name
//     path(tumor_bam) - Tumor Alignment File
//     path(tumor_bai) - Tumor Alignment Index
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Reference Index Files
//     path(dict_file) - Reference Dict File
//   parstr - Additional Parameters
//
// output:
//   tuple
//     val(pat_name) - Patient Name
//     val(dataset) - Dataset
//     path('*vcf') - Variant Call File

// require:
//   NORM_TUMOR_BAMS_BAIS
//   REF_W_INDICES
//   params.varscan2$varscan2_somatic_parameters

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}_${chr}"
  label 'varscan2_container'
  label 'varscan2_somatic'
  cache 'lenient'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/varscan2_somatic"


  input:
  tuple val(pat_name), val(dataset), val(norm_run), path(norm_pileup), val(tumor_run), path(tumor_pileup), val(chr)
  val parstr
  val suffix

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*snp.vcf"), emit: snv_vcfs
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*indel.vcf"), emit: indel_vcfs

  script:
  """
  CHR=`echo ${chr}`
  java -jar /opt/varscan/VarScan.jar somatic ${norm_pileup}/*\${CHR}.pileup ${tumor_pileup}/*\${CHR}.pileup ${dataset}-${pat_name}-${norm_run}_${tumor_run}.\${CHR}${suffix} ${parstr}
  """
}

workflow varscan2_somatic_parallel {
  take:
    pileups
    bed
    parstr
    suffix
    species
  main:
    if( species =~ /[Hh]uman|hs|HS|[Hh]omo/ ) {
        chrom_count = 25
    } else if( species =~ /[Mm]ouse|mm|MM|[Mm]us/ ) {
        chrom_count = 22
    }
    extract_chroms_from_bed(
      bed)
    pileups
      .combine(extract_chroms_from_bed.out.chroms_list.splitCsv(header: false, sep: '\n'))
      .set{ pileups_w_chr }
    varscan2_somatic_by_chr(
      pileups_w_chr,
      parstr,
      suffix)
    htslib_bgzip_somatic_snvs(
      varscan2_somatic_by_chr.out.snv_vcfs)
    bcftools_index_somatic_snvs(
      htslib_bgzip_somatic_snvs.out.bgzip_files,
      '')
    bcftools_index_somatic_snvs.out.vcfs_w_csis
      .groupTuple(by: [0, 1, 2, 3], size: chrom_count)
      .set{ snv_vcfs_by_pat }
    bcftools_concat_snvs(
      snv_vcfs_by_pat,
      '',
      'snvs.varscan2')

    varscan2_somatic_by_chr.out.indel_vcfs
    htslib_bgzip_somatic_indels(
      varscan2_somatic_by_chr.out.indel_vcfs)
    bcftools_index_somatic_indels(
      htslib_bgzip_somatic_indels.out.bgzip_files,
      '')
    bcftools_index_somatic_indels.out.vcfs_w_csis
      .groupTuple(by: [0, 1, 2, 3], size: chrom_count)
      .set{ indel_vcfs_by_pat }
    bcftools_concat_indels(
      snv_vcfs_by_pat,
      '',
      'indels.varscan2')
  emit:
    snv_vcfs = bcftools_concat_snvs.out.concatd_vcfs
    indel_vcfs = bcftools_concat_indels.out.concatd_vcfs
}
